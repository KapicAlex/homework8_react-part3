import { createSlice } from "@reduxjs/toolkit";
import { setProfile, resetError } from "../actions/profile";
import { errorMessages } from "../../enums/error/errors.enum";

const initialState = {
  user: null,
  error: {
    status: false,
    errorMessage: ""
  },
  preloader: false
};

const profileSlice = createSlice({
  name: "profile",
  initialState,
  extraReducers: {
    [setProfile.pending]: (state, action) => {
      state.error.status = false;
      state.error.errorMessage = "";
      state.preloader = true;
    },
    [setProfile.fulfilled]: (state, action) => {
      state.preloader = false;
      state.user = action.payload;
    },
    [setProfile.rejected]: (state, action) => {
      state.preloader = false;
      state.error.status = true;
      state.error.errorMessage = errorMessages.FAILED_LOGIN;
    },
    [resetError]: (state) => {
      state.error.status = false;
      state.error.errorMessage = "";
    }
  }
})

export default profileSlice.reducer;