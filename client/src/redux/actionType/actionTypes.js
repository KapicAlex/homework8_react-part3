export const SHOW_PRELOADER = "preloader/show-preloader";
export const HIDE_PRELOADER = "preloader/hide-preloader";

export const GET_MESSAGES = "messages/get-messages";
export const ADD_MESSAGE = "messages/add-message";
export const EDIT_MESSAGE = "messages/edit-message";
export const DELETE_MESSAGE = "messages/delete-message";
export const RESET_ERROR_MESSAGES = "messages/reset-error";

export const GET_USERS = "users/get-users";
export const ADD_USER = "users/add-user";
export const EDIT_USER = "users/edit-user";
export const DELETE_USER = "users/delete-user";
export const RESET_ERROR_USERS = "users/reset-error";

export const SET_PROFILE = "profile/get-profile";
export const RESET_ERROR_PROFILE = "profile/reset-error";