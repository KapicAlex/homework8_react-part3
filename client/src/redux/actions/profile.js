import { createAsyncThunk, createAction } from "@reduxjs/toolkit";
import { auth as authService } from "../../services/services";
import { SET_PROFILE, RESET_ERROR_PROFILE } from "../actionType/actionTypes";

const setProfile = createAsyncThunk(
  SET_PROFILE,
  async (payload) => await authService.login(payload)
);

const resetError = createAction(RESET_ERROR_PROFILE);

export { setProfile, resetError };