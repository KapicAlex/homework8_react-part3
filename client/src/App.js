import './App.css';
import Preloader from "./components/Preloader/Preloader";
import Main from "./components/Main/Main";
import MainHeader from "./components/MainHeader/MainHeader";
import Notifications from "./components/Notifications/Notifications";

import './App.css';

function App() {
  return (
    <div className="App">
      <MainHeader />
      <Main />
      <Preloader />
      <Notifications />
    </div>
  );
}

export default App;
