import React, { useEffect } from "react";
import moment from 'moment';
import { useSelector, useDispatch } from "react-redux";
import { getAllMessages } from "../redux/actions/actionsAll";
import { ROUTER } from "../enums/routes/routes"
import { useHistory } from "react-router-dom";
import Header from "./Header/Header";
import MessageList from "./MessageList/MessageList";
import MessageInput from "./MessageInput/MessageInput";
import "./chat.css";

const Chat = ({url}) => {
  const dispatch = useDispatch();
  const history = useHistory();
  const { messages, profile } = useSelector(state => ({
    messages: state.chat.messages,
    profile: state.profile.user
  }));

  useEffect(() => {
    dispatch(getAllMessages());
  }, [dispatch]);

  const getMainInfo = (messagesInfo) => {
    const allUserId = messagesInfo.map(message => message.userId);
    const allUsers = new Set(allUserId);
    const lastMessageDate =  messagesInfo[messagesInfo.length - 1]?.createdAt;
    return {
      usersSum: allUsers.size,
      allMessages: allUserId.length,
      lastMessageDate: moment(lastMessageDate).format("DD.MM.YYYY HH:mm"),
    }
  }

  const editLastMessage = (e) => {
    if(e.code === "ArrowUp") {
      const ownMessages = messages.filter(message => message.user === profile.username);
      const lastMessage = ownMessages.pop();
      if(lastMessage) history.push(ROUTER.messageEditor_$ID.replace(":id", lastMessage.id));;
    }
  }

  return (
    <div className="chat" onKeyUp={editLastMessage}>
      <Header data = {getMainInfo(messages)}/>
      <MessageList userId={profile.userId} />
      <MessageInput profile={profile} />
    </div>
  )
}

export default Chat;