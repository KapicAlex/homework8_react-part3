import React from "react";
import "./editUserCard.css";

const EditUserCard = ({ user, onDeleteUser, changeEditUser, profile }) => {
  const { username, userId, email } = user;
  const isDisabled = userId === profile.userId;

  return (
    <div className="user-card-edit">
      <div className="user-card-edit-info"><span>User name: </span>{username}</div>
      <div className="user-card-edit-info"><span>Email: </span>{email}</div>
      <div>
      <button
        className="edit-button"
        onClick={() => changeEditUser(user)}>Edit</button>
      <button
        className="delete-button"
        disabled={isDisabled}
        onClick={() => onDeleteUser(userId)}>
        Delete
      </button></div>
    </div>
  )
}

export default EditUserCard;