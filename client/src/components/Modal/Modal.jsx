import React, { useState, useEffect, useRef } from "react";
import { v4 as uuidv4 } from 'uuid';
import "./modal.css";
import { avatarDefault } from "../../enums/avatar/avatar.enum"

const Modal = ({ user, onEditUser, handleAddUser, onClose, isAddUser }) => {
  const { username, avatar, email, role } = user;
  const [userValue, setUserValue] = useState({
    username: "",
    avatar: "",
    email: "",
    password: "",
    role: "user"
  });
  const modalRef = useRef();
  const changeValue = (e) => {
    setUserValue({
      ...userValue,
      [e.target.name]: e.target.value
    });
  };

  const onClickClose = e => (e.target === modalRef.current) && onClose();

  const editUser = (e) => {
    e.preventDefault();
    const editedUser = {
      ...user,
      ...userValue,
      avatar: userValue.avatar ? userValue.avatar : avatarDefault,
      role: userValue.role,
      editedAt: new Date(Date.now())
    };
    console.log(userValue)
    onEditUser(editedUser);
    onClose();
  };

  const onAddUser = (e) => {
    e.preventDefault();
    const newUser = {
      ...userValue,
      avatar: userValue.avatar ? userValue.avatar : avatarDefault,
      role: userValue.role,
      userId: uuidv4(),
      createdAt: new Date(Date.now()),
      editedAt: ""
    };
    handleAddUser(newUser);
    onClose();
  };

  useEffect(() => {
    if (!isAddUser) {
      setUserValue({
        username,
        avatar,
        email,
        role
      });
    }
  }, [isAddUser, username, avatar, email, role]);


  return (
    <div
      ref={modalRef}
      onClick={onClickClose}
      className="edit-user-modal">
      <div className="edit-user-modal-body">
        <h2 className="edit-user-title">{isAddUser ? "Add user" : "Edit user"}</h2>
        <form className="edit-user-form" onSubmit={isAddUser ? onAddUser : editUser}>
          <label>
            Username
            <input
              className="edit-user-input"
              name="username"
              value={userValue.username}
              onChange={changeValue} />
          </label>
          <label>
            Avatar
            <input
              className="edit-user-input"
              name="avatar"
              value={userValue.avatar}
              onChange={changeValue} />
          </label>
          <label>
            E-mail
            <input
              className="edit-user-input"
              name="email"
              value={userValue.email}
              onChange={changeValue} />
          </label>
          {isAddUser && <label>
            password
            <input
              className="edit-user-input"
              name="password"
              required
              value={userValue.password}
              onChange={changeValue} />
          </label>}
          <label>
            Role
            <br />
            <select
              value={userValue.role}
              name="role"
              className="edit-user-select"
              required
              onChange={changeValue}>
              <option>user</option>
              <option>admin</option>
            </select>
          </label>
          <div className="edit-user-buttons">
            <button
              className="edit-user-button"
              type="submit">
              Ok
            </button>
            <button
              className="edit-user-close"
              type="button"
              onClick={onClose}
            >
              Cancel
            </button>
          </div>
        </form>
      </div>
    </div >
  )
}

export default Modal;