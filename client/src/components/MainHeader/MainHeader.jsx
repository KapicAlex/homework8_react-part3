import React from "react";
import Menu from "../Menu/Menu";
import { useSelector } from "react-redux";

import "./mainHeader.css";

const Header = () => {
  const user = useSelector(state => state.profile.user);
  const userIsAdmin = user ? user.role === "admin" : false;

  return (
    <div className={"main-header" + (userIsAdmin ? " isAdmin" : '')}>
      <Menu />
    </div>
  )
}

export default Header;