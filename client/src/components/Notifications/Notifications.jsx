import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { resetError } from "../../redux/actions/actionsAll";
import Snackbar from '@material-ui/core/Snackbar';
import Alert from '@material-ui/lab/Alert';
import "./notifications.css";

const Notifications = () => {
  const {
    profileError,
    usersError,
    chatError,
    profileErrorMessage,
    usersErrorMessage,
    chatErrorMessage } = useSelector(state => ({
      profileError: state.profile.error.status,
      usersError: state.users.error.status,
      chatError: state.chat.error.status,
      profileErrorMessage: state.profile.error.errorMessage,
      usersErrorMessage: state.users.error.errorMessage,
      chatErrorMessage: state.chat.error.errorMessage,
    }));
  const isError = profileError || usersError || chatError;
  const errorMessage = profileErrorMessage || usersErrorMessage || chatErrorMessage;
  const dispatch = useDispatch();

  return (
    <Snackbar
      open={isError}
      onClose={() => dispatch(resetError())}
      autoHideDuration={5000}
      onClick={() => dispatch(resetError())}
      anchorOrigin={{ vertical: 'top', horizontal: 'right' }}>
      <Alert severity="error">{errorMessage}</Alert>
    </Snackbar>
  );
}

export default Notifications;