import React, { useState } from "react";
import { v4 as uuidv4 } from 'uuid';
import { useDispatch } from "react-redux";
import { addMessage } from "../../redux/actions/actionsAll";
import "./messageInput.css";

const MessageInput = ({profile}) => {
  const [messageText, setMessageText] = useState("");
  const dispatch = useDispatch();

  const addText = (e) => {
    e.preventDefault();

    if(messageText) {
      const message = {
        id: uuidv4(),
        userId: profile.userId,
        avatar: profile.avatar,
        user: profile.username,
        text: messageText,
        createdAt: Date.now(),
        editedAt: "",
      };
      dispatch(addMessage(message));
      setMessageText("");
    }
  }

  return (
    <div className="message-input" >
      <form onSubmit={addText}>
        <input type="text"
          className="message-input-text"
          placeholder="Enter your message"
          value={messageText}
          onChange={e => setMessageText(e.target.value)} />
        <button className="message-input-button"
          type="submit">Send</button>
      </form>
    </div>
  )
}

export default MessageInput;