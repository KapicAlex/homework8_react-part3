import React from "react";
import { CircularProgress } from "@material-ui/core";
import { useSelector } from "react-redux";
import "./preloader.css"


const Preloader = () => {
  const { isLogin, isLoadMessages, isLoadUsers } = useSelector(state => ({
    isLogin: state.profile.preloader,
    isLoadMessages: state.chat.preloader,
    isLoadUsers: state.users.preloader
  }));

  const isLoad = isLogin || isLoadMessages || isLoadUsers;

  return (
    <div className="preloader" style={{display: isLoad ? "block" : "none"}}>
      <CircularProgress size="200px" />
    </div>
  )

}

export default Preloader;