import React from "react";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import { deleteMessage } from "../../redux/actions/actionsAll";
import { ROUTER } from "../../enums/routes/routes"
import "./ownMessage.css";

const OwnMesssage = ({ message }) => {
  const {id, text, createdAt, editedAt} = message;
  const time = editedAt ? new Date(editedAt) : new Date(createdAt);
  const dispatch = useDispatch();
  const history = useHistory();

  const onEditMessage = () => history.push(ROUTER.messageEditor_$ID.replace(":id", id));

  return (
    <div className="own-message">
      <div className="message-text">{text}</div>
      <div className="message-time">{time.toLocaleTimeString("ru-Ru", {timeStyle: 'short'})}</div>
      <div className="message-controls">
        <button className="message-edit" onClick={onEditMessage}><i className="fas fa-edit"></i></button>
        <button className="message-delete" onClick={() => dispatch(deleteMessage(id))}><i className="fas fa-trash-alt"></i></button>
      </div>
    </div>
  )
}

export default OwnMesssage;