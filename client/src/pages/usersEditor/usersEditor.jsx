import React, { useCallback, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { deleteUser, getAllUsers, editUser as editUserAction, addUser } from "../../redux/actions/actionsAll";
import EditUserCard from "../../components/EditUserCard/EditUserCard";
import Modal from "../../components/Modal/Modal";

import "./usersEditor.css";

const UserEditorPage = () => {
  const dispatch = useDispatch();
  const [editUser, setEditUser] = useState(null);
  const [isAddUser, setIsAddUser] = useState(false);
  const { users, profile } = useSelector(state => ({
    users: state.users.users,
    profile: state.profile.user
  }));

  const onAddUser = () => {
    setIsAddUser(true);
    setEditUser(true);
  };

  const onClose = () => {
    setIsAddUser(false);
    setEditUser(null);
  };

  const changeEditUser = (user) => setEditUser(user);

  const onDeleteUser = useCallback((userId) => {
    dispatch(deleteUser(userId));
  }, [dispatch]);

  const onEditUser = useCallback((user) => {
    dispatch(editUserAction(user));
    dispatch(getAllUsers());
  }, [dispatch]);

  const handleAddUser = useCallback((user) => {
    dispatch(addUser(user));
  }, [dispatch]);

  useEffect(() => {
    dispatch(getAllUsers());
  }, [dispatch]);

  return (
    <div className="user-list">
      {editUser && <Modal
                      isAddUser={isAddUser}
                      user={editUser}
                      onEditUser={onEditUser}
                      handleAddUser={handleAddUser}
                      onClose={onClose} />}
      <h1 className="user-list-title">All users</h1>
      <div className="user-list-buttons">
        <button
          className="add-user-button"
          onClick={onAddUser}>
          Add user
        </button>
      </div>
      <div className="users">
        {users.map(user => <EditUserCard
                              key={user.userId}
                              user={user}
                              onDeleteUser={onDeleteUser}
                              changeEditUser={changeEditUser}
                              profile={profile} />)}
      </div>
    </div>
  )
}

export default UserEditorPage;