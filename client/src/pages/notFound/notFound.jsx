import React from "react";
import { NavLink } from "react-router-dom";
import { ROUTER } from "../../enums/routes/routes";
import "./notFound.css";

const NotFoundPage = () => {
return (
    <div className="notFound-page">
      <h1 className="notFound-page-title">This page not exist!</h1>
      <NavLink className="notFound-page-link" to={ROUTER.chat}>Go to homepage</NavLink>
    </div>
  )
}

export default NotFoundPage;