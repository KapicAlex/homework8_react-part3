import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import { editMessage } from "../../redux/actions/actionsAll";
import { ROUTER } from "../../enums/routes/routes";
import "./messageEditor.css";

const MessageEditorPage = ({ match }) => {
  const messages = useSelector(state => state.chat.messages);
  const [message, setMessage] = useState(null);
  const [textValue, setTextValue] = useState("");
  const { id } = match.params;
  const dispatch = useDispatch();
  const history = useHistory();

  const onEditMessage = (e) => {
    e.preventDefault();

    if(textValue) {
      const newMessage = {
      ...message,
      text: textValue,
      editedAt: Date.now()
      }
      dispatch(editMessage(newMessage));
    }else{
      const newMessage = message
      dispatch(editMessage(newMessage));
    }
    history.push(ROUTER.chat);
  }

  const onClose = () => history.push(ROUTER.chat);

  useEffect(() => {
    if (messages.length) {
      const editMessage = messages.find(item => item.id === id);
      setMessage(editMessage);
      setTextValue(editMessage.text);
    }
  }, [messages, id]);

  return (
    <div className="edit-message-page">
      <div className="edit-message-body">
        <h2 className="edit-message-title">Edit message</h2>
        <form className="edit-message-form" onSubmit={onEditMessage}>
          <textarea
            className="edit-message-input"
            value={textValue}
            onChange={e => setTextValue(e.target.value)} />
          <div className="edit-message-buttons">
            <button
              className="edit-message-button"
              type="submit">
              Edit
            </button>
            <button
              className="edit-message-close"
              type="button"
            onClick={onClose}>
              Cancel
            </button>
          </div>
        </form>
      </div>
    </div>
  )
}

export default MessageEditorPage;