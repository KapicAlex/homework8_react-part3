import React, { useEffect } from "react";
import UserCard from "../../components/UserCard/UserCard";
import { useDispatch, useSelector } from "react-redux";
import { getAllUsers } from "../../redux/actions/actionsAll";

import "./userList.css";

const UserListPage = () => {
  const dispatch = useDispatch();
  const users = useSelector(state => state.users.users);

  useEffect(() => {
    dispatch(getAllUsers());
  }, [dispatch]);


  return (
    <div className="user-list">
      <h1 className="user-list-title">All users</h1>
      <div className="users">
        {users.map(user => <UserCard key={user.userId} user={user} />)}
      </div>
    </div>
  )
}
export default UserListPage;