import authRoutes from "./authentication/auth.routes.js";
import messagesRoutes from "./messages/messages.routes.js";
import userRoutes from "./user/user.routes.js";

export default (app) => {
  app.use("/api/auth/login", authRoutes);
  app.use("/api/users", userRoutes);
  app.use("/api/messages", messagesRoutes);
}